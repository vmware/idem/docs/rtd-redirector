# !ARCHIVED!

This project has been archived, along with all other POP and Idem-based projects.
- For more details: [Salt Project Blog - POP and Idem Projects Will Soon be Archived](https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/)

# Sphinx Redirector Temp Repository

Use this repository as a way to apply redirects to ReadTheDocs.

## Prereqs

- Make sure project is already publishing documentation to a new `docs.idemproject.io/*` endpoint

## Submit an MR to this repo

MR should include:

- Updating the following lines to have the correct, new URL locations:
  - https://gitlab.com/vmware/idem/docs/rtd-redirector/-/blob/master/conf.py#L22
  - https://gitlab.com/vmware/idem/docs/rtd-redirector/-/blob/master/conf.py#L31
- Update the project name to be the name of the plugin being migrated:
  - https://gitlab.com/vmware/idem/docs/rtd-redirector/-/blob/master/conf.py#L9

## Apply to RTD Project Settings

After the MR has been merged in:

- Got to the RTD project in readthedocs portal
- Go to **Admin**
  - **Settings**
    - **Repository URL**: Change to `https://gitlab.com/vmware/idem/docs/rtd-redirector`
    - Hit **Save**
  - Go to **Advanced Settings**
    - Ensure **Requirements file** is: `requirements.txt`
    - Disable PDF and EPUB builds
    - Hit **Save**
  - Go to **Redirects**
    - **Add Redirect**
      - **Redirect Type**: `Exact Redirect`
      - **From URL**: `/$rest`
      - **To URL**: `https://docs.idemproject.io/<idem-plugin-name>/` (ex. `https://docs.idemproject.io/idem-aws/`)
      - Hit **Save**
- Leave **Admin** settings. Go to **Builds**
  - Press **Build Version:** with `latest` selected
- New docs should be built with redirects pointing to new location!
